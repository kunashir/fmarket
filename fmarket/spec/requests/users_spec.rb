require 'spec_helper'

describe "Users" do
  describe "GET /users" do
    it "works! (now write some real specs)" do
      # Run the generator again with the --webrat flag if you want to use webrat methods/matchers
      get users_path
      response.status.should be(200)
    end
  end

  describe "POST new" do
    before { visit new_user_path}
    let(:user) { FactoryGirl.create(:user)}
     before do
      fill_in "email",                  with: user.email
      fill_in "password",               with: user.password
      fill_in "password_confirmation",  with: user.password_confirmation 
      click_button "sign_up"
    end
    it "create new user" do
      it { should have_selector('title', text: user.name) }
      it { should have_link('Profile', href: user_path(user)) }
      it { should have_link('Sign out', href: signout_path) }
      it { should_not have_link('Sign in', href: signin_path) }
    end
  end
end
