 
FactoryGirl.define do
  factory :company do
    name   "Some company"
    inn    "2222999969"
  end

  factory :reciver, class: Company do
    name  "Reciver price company"
    inn   "1234567890"
  end

  factory :bad_reciver, class: Company do
    name  "BAD Reciver price company"
    inn   "1234567811"
  end

  # company_stubb  = FactoryGirl.build(:company)
  # reciver_stubb  = FactoryGirl.build(:reciver)

  factory :price do 
    start_date  Date.new(2013, 1,1)
    end_date    Date.new(2013, 2, 2)
    company     #company_stubb #FactoryGirl.create(:company)
    reciver     #reciver_stubb #FactoryGirl.create(:reciver)

  end

  factory :order do 
    #contractor  company_stubb #FactoryGirl.create(:company)
    association :contractor, factory: :company
    association :owner, factory: :reciver
    #owner       reciver_stubb #FactoryGirl.create(:reciver)
    date        Date.today
  end

  factory :user do
    email                 "admin@apteka-s.ru" 
    password              "123456"
    password_confirmation "123456"
  end

end
