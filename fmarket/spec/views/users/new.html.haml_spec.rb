require 'spec_helper'

describe "users/new" do
  it "renders form to new user" do
    render
    rendered.should have_selector("form",
      :method => "post",
      :action => users_path# "/sessions"
      ) do |form|
      form.should have_selector("input", :class => "btn", :name => "commit", :type => "submit")
      form.should have_selector("input",  :type => "text", :name => "email")
      form.should have_selector("input",  :type => "password", :name => "password") 
      form.should have_selector("input",  :type => "password", :name => "password_confirmation")
    end 
  end
end
