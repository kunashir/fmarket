require 'spec_helper'

describe "sessions/new" do
  it "displays  the text input reg data" do
    render
    rendered.should contain(t(:input_reg_data))
  end

  it "renders form to login" do
    render
    rendered.should have_selector("form",
      :method => "post",
      :action => sessions_path# "/sessions"
      ) do |form|
      form.should have_selector("input", :class => "btn", :name => "commit", :type => "submit", :value => "Login")
    end 
  end
end
#<input class="btn" name="commit" type="submit" value="Login" />