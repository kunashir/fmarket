require 'spec_helper'

describe OrderContent do
   before(:each) do
    @att = {:order => FactoryGirl.create(:order), :price_content =>  mock_model("PriceContent"), :qnt => 10 }
    
    @order_cont = OrderContent.new(@att)
  end

  it "should create a new instance given valid attr" do
    #Price.create!(@price_att)
    @order_cont.save
  end

  it "should require a order" do
    @order_cont.order = nil
    @order_cont.should_not be_valid
  end

  it "should require a price contetn" do
    @order_cont.price_content = nil
    @order_cont.should_not be_valid
  end

  it "should require a  qnt" do
    @order_cont.qnt = nil
    @order_cont.should_not be_valid
  end

  it "should require a not null qnt" do
    @order_cont.qnt = 0
    @order_cont.should_not be_valid
  end


end
