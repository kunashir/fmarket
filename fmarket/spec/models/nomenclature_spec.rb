#ecoding: utf-8
require 'spec_helper'

describe Nomenclature do
  before(:each) do
    @attr = { :name => "Какое-то лекарство или другая шняга"}
    @company_att = { :name => "Some company", :inn => "2222999969"}
    @some_company = Company.new(@company_att)
  end

  it "should create a new instance given valid attr" do
    Nomenclature.create!(@attr.merge(:company => @some_company))
  end

  it "should reject if empty company" do
    nom = Nomenclature.new(@attr)
    nom.should_not be_valid
  end

  it "should require a name" do
    nom = Nomenclature.new(@attr.merge(:name => ""))
    nom.should_not be_valid
  end

  it "should find or create item with some parametrs" do
    nom = Nomenclature.get_item(@attr[:name], @some_company)
    nom.name == @attr[:name]
  end

end
