#ecoding: utf-8
require 'spec_helper'

describe Manufacturer do
  before(:each) do
   
    @attrContr = { :name => "Россия", :full_name => "Российская Федерация", :alpha2 => "RU"}
    @country = WorldClassifier.create(@attrContr)#TODO:refactoring through factory
     @attr = { :name => "Some manuf", :country => @country}

  end

  it "should create a new instance given valid attr" do
    Manufacturer.create!(@attr)
  end

  it "should belongs to country" do
    manuf = Manufacturer.new(@attr)
    manuf.country.should_not be_nil
  end

  it "should find or creat item with some attributes" do
    some_manuf = Manufacturer.get_item(@attr[:name], @attrContr[:name])
    some_manuf == @attr[:name]
  end
end
