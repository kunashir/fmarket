#ecoding: utf-8
require 'spec_helper'

describe Series do

  before(:each) do
    @attr = {:serial_code => "20130101", :expiration_date => "2013-01-01"}
    @attr_nom = { :name => "Какое-то лекарство или другая шняга"}
    #@some_company = Company.new( :name => "Some company", :inn => "2222999969")
    #let(:some_company) {FactoryGirl.create(:company)}
    some_company = FactoryGirl.build(:company)
    @nom = Nomenclature.new(@attr_nom.merge(:company => @some_company))
  end

  it "should create a new instance given valid attr" do
    Series.create!(@attr.merge(:nomenclature => @nom))
  end

  it "should require a serial_code" do
    series = Series.new(@attr.merge(:nomenclature => @nom, :serial_code => ""))
    series.should_not be_valid
  end

  it "should require a nomenclature" do
    series = Series.new(@attr.merge( :serial_code => ""))
    series.should_not be_valid
  end


end
