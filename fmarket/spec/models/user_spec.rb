require 'spec_helper'

describe User do
  before(:each) do
    @attr =  {  :email => "some@gmail.com",
                :password => "foobar",
                :password_confirmation => "foobar"}
    # @company_att = { :name => "Some company", :inn => "2222999969"}
    # @price_att = {:start_date => Date.new(2013, 1,1), :end_date => Date.new(2013, 2, 2)}
    @user = User.new(@attr)
  end

  it "should create a new instance given valid attr" do
    @user.company = mock_model("Company")
    #User.create!(@attr)
    @user.save
  end

  it "should require a email address" do 
    @user.email = ""
    @user.should_not be_valid
  end

  it "should require a valid email address" do
    @user.email = "kkk@kkk"
    @user.should_not be_valid
  end

  describe "user's company" do
    it "may belong to company" do
      @user.should_not be_valid
    end
  end

  describe "accessible attributes" do 
    it "should not allow access to admin field" do 
      expect do 
        User.new(admin: true)
      end.to raise_error(ActiveModel::MassAssignmentSecurity::Error)
    end
  end

  # describe "access to pricies" do

  #   before(:each) do
  #     @some_company = Company.new(@company_att) 
  #     #create price to company
  #     @some_company.prices.build(@price_att)
  #   end

  #   it "should reject pricies list if user don't belong to company" do
  #     #some_company = Company.new(@company_att)
  #     #some_company.prices.build(@price_att)
  #     some_user = User.new(@attr)
  #     some_user.get_prices(@some_company).should be_nil
  #   end

  #   it "must been if user belong to company" do
  #     some_user = User.new(@attr)
  #     some_user.company = @some_company
  #     some_user.get_prices(@some_company).should_not be_nil
  #   end

  # end
end
