require 'spec_helper'

describe Order do
  before(:each) do
    @order_att = {:owner => FactoryGirl.create(:reciver), :contractor => FactoryGirl.create(:company), :date => Date.today }
    
    @order = Order.new(@order_att)
  end

  it "should create a new instance given valid attr" do
    #Price.create!(@price_att)
    @order.save
  end

  it "should require a owner" do
    @order.owner = nil
    @order.should_not be_valid
  end

  it "should require a contractor" do
    @order.contractor = nil
    @order.should_not be_valid
  end  

  it "should require a date" do
    @order.date = nil
    @order.should_not be_valid
  end
end
