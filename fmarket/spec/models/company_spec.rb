require 'spec_helper'

describe Company do
  before(:each) do
    @attr = { :name => "Some company", :inn => "2222999969"}
  end

  it "should create a new instance given valid attr"do
    Company.create!(@attr)
  end

  it "should have a name" do
    no_name_company = Company.new(@attr.merge(:name => ""))
    no_name_company.should_not be_valid
  end

  it "should have a inn" do
    no_inn = Company.new(@attr.merge(:inn => ""))
    no_inn.should_not be_valid
  end

  it "should generate subcode" do 
    no_subcode = Company.new(@attr)
    no_subcode.subcode.should_not be_nill
  end

  describe "inn validations" do

    it "should reject invalid inn" do
      invalid_inn = Company.new(@attr.merge(:inn => "FFF1123"))
      invalid_inn.should_not be_valid
    end

    it "should reject short INN" do
      short_inn = Company.new(@attr.merge(:inn => "12345678"))
      short_inn.should_not be_valid
    end

    it "should reject long INN" do
      long_inn = Company.new(@attr.merge(:inn => "123456789123456"))
      long_inn.should_not be_valid
    end
  end
end
