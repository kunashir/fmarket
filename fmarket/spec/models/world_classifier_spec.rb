#ecoding: utf-8
require 'spec_helper'

describe WorldClassifier do
  before(:each) do
    @attr = { :name => "Россия", :full_name => "Российская Федерация", :alpha2 => "RU"}
    
  end

  it "should create a new instance given valid attr" do
    WorldClassifier.create!(@attr)
  end

  it "should require name" do
    no_name = WorldClassifier.new(@attr.merge(:name => ""))
    no_name.should_not be_valid
  end

  it "should find or create item with specification name" do
    cur_country = WorldClassifier.get_item(@attr[:name])
    cur_country.name == @attr[:name]
  end
  
end
