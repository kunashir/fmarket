require 'spec_helper'

describe PriceContent do
  before(:each) do
    @price_content = PriceContent.new(:prices => FactoryGirl.create(:price),
                                      :nomenclature => mock_model("Nomenclature"),
                                      :series => mock_model("Series"),
                                      :manufacturer => mock_model("Manufacturer"),
                                      :price => 12.2, :qnt => 100)
  end

  it "should create a new instance given valid attr" do
    @price_content.save
  end

  it "should require a prices" do
    @price_content.prices = nil
    @price_content.should_not be_valid
  end

  it "should require a nomenclature"  do
    @price_content.nomenclature = nil
    @price_content.should_not be_valid
  end


  it "should require a series"do
    @price_content.series = nil
    @price_content.should_not be_valid
  end

  it "should require a price" do
    @price_content.price = nil
    @price_content.should_not be_valid
  end

  it "should require a qnt" do
    @price_content.qnt = nil
    @price_content.should_not be_valid
  end
end
