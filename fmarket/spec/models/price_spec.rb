require 'spec_helper'

describe Price do
  before(:each) do
    @company_att = { :name => "Some company", :inn => "2222999969"}
    price_att = {:start_date => Date.new(2013, 1,1), 
                  :end_date => Date.new(2013, 2, 2)
                  }
    @price = Price.new(price_att)
  end

  it "should create a new instance given valid attr" do
    #Price.create!(@price_att)
    @price.save
  end

  it "must been belongs to company" do
    #company = Company.new(@company_att)
    #price   = Price.new(@price_att)
    @price.company = mock_model("Company")#company
    @price.company.should_not be_nil
  end

  it "should require a start date" do
    @price.start_date = nil
    @price.should_not be_valid
  end

  describe "Prices content" do 
    it "should return some special price for some customer company" do
      price = FactoryGirl.create(:price)
      Price.get_prices(price.reciver, price) == price #maybe return array?
    end

    it "should return all avalible prices" do 
      price = FactoryGirl.create(:price)
      Price.get_prices(price.reciver) == price #get price without reference to a particular to the price
    end 
     
    it "should reject if current company not equal prices reciver" do
      bad_reciver = FactoryGirl.create(:bad_reciver)
      price = FactoryGirl.create(:price)
      Price.get_prices(bad_reciver, price) == []#try to get the prices
    end

    it "should return own prices" do
      owner_prices = FactoryGirl.create(:company)
      @price.company = owner_prices
      @price.save
      Price.own_prices(owner_prices) == [@price]
    end
    
    it "should return own content "

    # it "should return prices content for prices list or of  all avalible prices" do
    #   price = FactoryGirl.create(:price)
    #   Price.get_price_content(price.reciver) == price.price_content
    # end

    it "should load data from xml" do
      price = FactoryGirl.build(:price)
      price.load_from_file("spec/data/test.xml") != []
    end
  end

  describe "should create a order" do
    it "should create orders for each prices owner"
  end
end
