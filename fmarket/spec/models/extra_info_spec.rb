#ecoding: utf-8
require 'spec_helper'

describe ExtraInfo do
  before(:each) do
    @attr = {:name => "some extra information"}
    @attr_nom = { :name => "Какое-то лекарство или другая шняга"}
    @some_company = Company.new( :name => "Some company", :inn => "2222999969")
    @nom = Nomenclature.new(@attr_nom.merge(:company => @some_company))
  end

  it "should create a new instance given valid attr" do
    ExtraInfo.create!(@attr.merge(:nomenclature => @nom))
  end

  it "should require a name" do
    extra = ExtraInfo.new(@attr.merge(:nomenclature => @nom, :name => ""))
  end

  it "should require a nomenclature" do
    extra = ExtraInfo.new(@attr)
  end
end
