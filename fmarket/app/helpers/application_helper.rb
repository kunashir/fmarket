# coding: utf-8
module ApplicationHelper

  def save_file(file_content_from_param)
    #takes a file name and contents of the file 
    uploaded_io = file_content_from_param
    full_path_to_file = Rails.root.join('public', 'uploads', uploaded_io.original_filename)
    File.open(full_path_to_file,'w') do |file|
      text = uploaded_io.read
      puts text
      file.write(text.force_encoding("UTF-8"))
    end
    return full_path_to_file
  end
end
