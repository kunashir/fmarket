require 'xml'
class Price < ActiveRecord::Base
  attr_accessible :end_date, :start_date
  belongs_to  :company
  belongs_to  :reciver, :class_name => "Company", :foreign_key => :reciver_id
  has_many    :price_content, :dependent => :destroy, :foreign_key => "prices_id"

  validates   :start_date, :presence => true #end_date may be empty

  def self.own_prices(company) #for owner prices 
    Price.where("company_id = ?", company)
  end

  def self.get_prices(company, prices=nil) #for customers
    if prices
      Price.where("(reciver_id = ? AND id = ?) AND (company_id <> ?)", company, prices, company)
    else
      Price.where("(reciver_id = ? OR reciver_id IS NULL)  AND (company_id <> ?)", company, company) #TODO: add filter for actiual priceis
    end
  end 

  def get_content(filter={})
    PriceContent.get_content(self, filter)
  end
  # def get_price_content#(company, actual_date = Time.now)
  #   #must return only own content
  #   @price_content = PriceContent.get_own_content(self) #PriceContent.get_avalible_content(company, actual_date)
  # end

  def load_from_file(filename)
    create_content(read_xml(filename))
    #PriceContent.where("prices_id = ?", self)
    #PriceContent.get_own_content(self)
    self.get_content
  end

private
  def create_content(hPrice)
    #hPrice - hash with data to price contetn
    arr_content = hPrice["price_content"] || []
    return if arr_content.size  == 0
    ActiveRecord::Base.transaction do
      arr_content.each do |item|
        price_content = PriceContent.new
        price_content.nomenclature  = Nomenclature.get_item(item["nomenclature"], self.company) #find or create
        price_content.prices        = self
        price_content.price         = item["price"].to_f
        price_content.qnt           = item["qnt"].to_f
        price_content.series        = Series.get_item(item["series"]) if item["series"]
        price_content.manufacturer  = Manufacturer.get_item(item["manufacturer"], item["country"])
        price_content.save!
      end
    end
  end

  def read_xml(filename)
    parser = XML::Parser.file(filename.to_s)
    doc = parser.parse
    doc_hash = {}
    doc_hash[:root] = doc.root.attributes.to_h
    doc.root.children.each do |table_part| 
      puts table_part.name
      array_of_table_part = Array.new
      table_part.children.each do |str|
        attrs = str.attributes.to_h
          array_of_table_part << attrs if attrs.size != 0
      end
      doc_hash[table_part.name] = array_of_table_part
    end
    return doc_hash
  end
end
