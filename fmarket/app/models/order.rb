require 'xml'
class Order < ActiveRecord::Base
  attr_accessible :contractor, :date, :owner, :status

  belongs_to  :contractor,  :class_name => "Company", :foreign_key => :contractor_id
  belongs_to  :owner,       :class_name => "Company", :foreign_key => :owner_id

  validates   :contractor,  :presence => true
  validates   :owner,       :presence => true
  validates   :date,        :presence => true

  STATUS = {
  	0 => "preparation",
  	1 => "submitted",
  	2 => "accepted",
  	3 => "assembly",
    4 => "shipped"
  }.freeze

  def self.create_orders(cur_company, hash_orders)
  	#get data of price contetn and extra data from prices
  	price_content_with_price_data = PriceContent.get_content_for_order(hash_orders.keys)
  	orders_hash = {} #keys - it is prices, value - it is array of price content items
  	price_content_with_price_data.each do |price_content|
  		orders_hash[price_content.prices] = (orders_hash[price_content.prices] || []) << price_content
  	end
  	#creating orders!!!
  	orders_hash.each do |order_data, content|
  		new_order = Order.new(:owner => cur_company, :contractor => order_data.company, :status => 0, :date => Date.today)
  		if new_order.save!
  			content.each do |content_item|
  				order_cont = OrderContent.new(:order => new_order, :price_content => content_item, :qnt => hash_orders[content_item.id])
  				return false if !order_cont.save
  			end
  		else
  			return false
  		end
  	end
  	return true
  end

  def self.for_contractors(cur_company)
  	Order.where("owner_id = ?", cur_company)
  end

  def self.from_customers(cur_company)
    Order.where("contractor_id = ?", cur_company)
  end

  def get_content
    OrderContent.get_data(self)
  end

  def create_xml
    #xml schema: root = general date of order
    #<order_content> - list of nomenclature
    xml = XML::Document.new
    
    root = XML::Node.new("doc")
    root["type"] = "order"
    root["date"] = self.date.to_s
    root["customer"] = self.owner.to_s

    order_content = XML::Node.new("order_content")
    self.get_content.each do |item| #itme it is not object - it is hash
      cur_content = XML::Node.new("content")
      cur_content["nomenclature"] = item[:nomenclature]
      cur_content["manufacturer"] = item[:manufacturer]
      cur_content["series"] = item[:series] || ""
      cur_content["qnt"] = item[:qnt_order].to_s
      cur_content["price"] = item[:price].to_s
      order_content << cur_content
    end

    root << order_content

    xml.root = root

    xml.save(self.owner.to_s + ".xml", :indent => true, :encoding => XML::Encoding::UTF_8)

    outputfile = File.open(self.owner.to_s + ".xml")
    # outputfile.puts xml.to_s 
    # outputfile.close
    return outputfile#.path
  end
end
