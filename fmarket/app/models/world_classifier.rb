class WorldClassifier < ActiveRecord::Base
  # attr_accessible :title, :body
  attr_accessible :name, :full_name, :alpha2
  validates :name, :presence => true

  def self.get_item(name)
    cur_country = WorldClassifier.where("name = ?", name).first
    if !cur_country
      WorldClassifier.create(:name => name)
    end
  end
end
