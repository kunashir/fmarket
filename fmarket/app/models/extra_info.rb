class ExtraInfo < ActiveRecord::Base
  attr_accessible :name, :nomenclature
  belongs_to :nomenclature

  validates :name,          :presence => true
  validates :nomenclature,  :presence => true
end
