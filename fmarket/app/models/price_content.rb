class PriceContent < ActiveRecord::Base
  attr_accessible :manufacturer, :nomenclature, :price, :prices, :qnt, :series

  belongs_to :manufacturer, :class_name => "Manufacturer", :foreign_key => :manufacture_id
  belongs_to :nomenclature
  belongs_to :prices, :class_name => "Price"
  belongs_to :series
  has_many   :order_content

  validates   :nomenclature,  :presence => true
  validates   :price,         :presence => true
  validates   :prices,        :presence => true
  validates   :series,        :presence => true
  validates   :qnt,           :presence => true

  def self.get_content(price, search_option = {})
    output = []
    puts search_option
    nom_filter = search_option[:nomenclature] || "%"
    nomenclatures = Nomenclature.filter(nom_filter, price.company)
    PriceContent.includes(:nomenclature, :manufacturer).where("prices_id = ? AND nomenclature_id in (?)", price, nomenclatures).each do |pc|
    #PriceContent.includes(:nomenclature).where("prices_id = ? ", price).each do |pc|
      attr_hash = pc.attributes
      attr_hash[:nomenclature] = pc.nomenclature.name
      attr_hash[:manufacturer] = pc.manufacturer.name
      output<<attr_hash
    end
    #puts output
    return output

  end

  #inputarray of id's, 
  #return array of object PriceContent and leny load Prices
  def self.get_content_for_order(arr_ids) 
    return [] if arr_ids.size == 0
    #PriceContent.includes(:prices).where("id in (?)", arr_ids)
    PriceContent.includes(:prices).joins(:order_content).where("order_contents.id in (?)", arr_ids)
  end
  # def self.get_avalible_content(company, actual_date)
  # end
end
