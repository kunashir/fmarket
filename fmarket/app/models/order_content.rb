class OrderContent < ActiveRecord::Base
  attr_accessible :order, :price_content, :qnt

  belongs_to  :order
  belongs_to  :price_content,  :class_name => "PriceContent", :foreign_key => :price_content_id

  validates   :order,           :presence => true
  validates   :price_content,   :presence => true
  validates   :qnt,             :presence => true, :numericality => {:only_integer => true, :greater_than => 0}

  def self.get_data(order)
    order_content = OrderContent.where("order_id = ?", order) #get all item's of current order
    by_price_content = Hash[ order_content.map { |cont| [cont.price_content, cont.qnt] }]
    arr_price_content = PriceContent.get_content_for_order(order_content)
    output = []
    arr_price_content.each do |pc_item| #pc - price content
      attr_hash = pc_item.attributes
      attr_hash[:nomenclature]  = pc_item.nomenclature.name
      attr_hash[:manufacturer]  = pc_item.manufacturer.name
      attr_hash[:qnt_order]     = by_price_content[pc_item]
      output << attr_hash
    end
    return output
  end
end
