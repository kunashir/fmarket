class Manufacturer < ActiveRecord::Base
  attr_accessible :code, :name, :country
  belongs_to :country, :class_name => "WorldClassifier", 
          :foreign_key => "worldclassifier_id"

  def self.get_item(name, country_name)
    country = WorldClassifier.get_item(country_name)
    manuf = Manufacturer.where("name = ? AND worldclassifier_id = ?", name, country).first
    if !manuf
      Manufacturer.create(:name => name, :country => country)
    end
  end
end
