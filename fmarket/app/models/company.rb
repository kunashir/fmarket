class Company < ActiveRecord::Base
  acts_as_nested_set
  attr_accessible :buyer, :depth, :inn, :lft, :name, :parent_id, :rgt, :supplier

  validates :inn,   :presence => true, :format => {:with => /\d{10,12}/},
                       :length => {:within => 10..12}
  validates_uniqueness_of :inn
  validates :name,  :presence => true
  validates :subcode, :length => {:within => 3..3}
  has_many  :prices
  has_many  :users
  before_save :set_subdiv_code
  before_update :set_subdiv_code

  def last_subdiv
    Company.where("parent_id", self).last || self 
  end

  def is_head?
    !self.child?
  end
  

#private
  def set_subdiv_code
    return self.subcode if self.subcode
    last_code = (self.parent.last_subdiv.subcode.to_i if self.parent) || 0
    subcode = (last_code + 1).to_s
    self.subcode = ("0"*(3 - subcode.length)) + subcode
    puts "SUB CODE ===========#{subcode}"
  end

  def to_s
    self.name.to_s 
  end

end
