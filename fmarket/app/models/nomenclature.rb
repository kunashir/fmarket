class Nomenclature < ActiveRecord::Base
  attr_accessible :company, :name
  belongs_to  :company
  has_many    :series

  validates :name, :presence => true
  validates :company, :presence => true

  def self.get_item(nom_name, cur_company)
    #try to find in db
    item = Nomenclature.where("name = ? AND company_id = ?", nom_name, cur_company).first
    if !item # if not find, create new
      item = Nomenclature.new(:name => nom_name, :company => cur_company)
      item.save
    end
    return item
  end

  def self.filter(filter_text, cur_company)
    if filter_text == "%"
      Nomenclature.select("id").where("company_id = ?",  cur_company)
    else
      Nomenclature.select("id").where("name LIKE ? AND company_id = ?", filter_text, cur_company)
    end 
  end
end
