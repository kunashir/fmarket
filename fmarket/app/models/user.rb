class User < ActiveRecord::Base
  authenticates_with_sorcery!
 
  # attr_accessible :title, :body
  attr_accessible :email, :password, :password_confirmation, :name
  
  belongs_to :company

  validates_confirmation_of :password
  validates_presence_of :password, :on => :create
  #validates_presence_of :email
  validates :company, :presence => true
  validates_uniqueness_of :email

  email_regex = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i

  validates :email, :presence => true, :format => {:with => email_regex}
  #validates :company, :presence => true
  def print_name
    company_name = (self.company.name if self.company) || I18n.t(:no_company)
    "#{self.name}, #{company_name}"
  end

  def self.users_company(company)
    list_of_company = company.children
    #list_of_company << company
    User.where("company_id IN (?) OR company_id = ?", list_of_company, company)
  end

  def string_status
    return I18n.t(:block_unset) if self.block?
    return I18n.t(:block_set) unless self.block?
  end

end
