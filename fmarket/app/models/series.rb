class Series < ActiveRecord::Base
  attr_accessible :expiration_date, :nomenclature, :serial_code

  belongs_to :nomenclature

  validates :serial_code, :presence => true 
  validates :nomenclature, :presence => true

  def self.get_item(serial_code)
    cur_series = Series.where("serial_code = ?", serial_code).first
    if !cur_series
      Series.create(:serial_code => serial_code)
    end
  end

end
