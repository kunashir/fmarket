# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

get_data = (location) ->
  jQuery("#order_content").jqGrid
    url: location + "/get_content"
    datatype: "json"
    colNames: ["id", "nomenclature", "manufacturer", "price", "prices", "qnt", "series"]
    colModel: [
      name: "id"
      index: "id"
      width: 90
      hidden: true
    ,
      name: "nomenclature"
      index: "nomenclature"
      width: 90
    ,
      name: "manufacturer"
      index: "manufacturer"
      width: 100
    ,
      name: "price"
      index: "price"
      width: 80
      align: "right"
    ,
      name: "prices_id"
      index: "prices_id"
      width: 80
      align: "right"
    ,
      name: "qnt_order"
      index: "qnt_order"
      width: 80
      align: "right"
    ,
      name: "series_id"
      index: "series_id"
      width: 150
      sortable: false
    ]
    rowNum: 10
    rowList: [10, 20, 30]
    pager: "#order_to_content"
    sortname: "id"
    viewrecords: true
    sortorder: "desc"
    jsonReader:
      repeatitems: false

    caption: "Price data"

  jQuery("#order_content").jqGrid "navGrid", "#order_to_content",
    edit: false
    add: false
    del: false

(($) ->
  $(document).ready ->
    get_data window.location

) jQuery