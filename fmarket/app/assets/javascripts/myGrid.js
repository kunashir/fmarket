var lastsel;
function get_data (location)
{
  jQuery("#price_content").jqGrid(
      { url:      location + '/get_content', 
        datatype: "json", 
        colNames:['id','nomenclature', 'manufacturer', 'price','prices','qnt','series'], 
        colModel:[ 
          {name:'id',           index:'id',            width:90},
          {name:'nomenclature', index:'nomenclature',  width:90} ,
           {name:'manufacturer_id', index:'manufacturer_id',  width:100},
           {name:'price',        index:'price',         width:80, align:"right"}, 
           {name:'prices_id',       index:'prices_id',        width:80, align:"right"}, 
           {name:'qnt',          index:'qnt',           width:80, align:"right"}, 
           {name:'series_id',       index:'series_id',        width:150, sortable:false} 
          ], 
          rowNum:10, 
          rowList:[10,20,30], 
          pager: '#pager_to_content', 
          sortname: 'id', 
          viewrecords: true, 
          sortorder: "desc", 
          jsonReader: { repeatitems: false },
          caption:"Price data" }); 
    jQuery("#price_content").jqGrid('navGrid','#pager_to_content',{edit:false,add:false,del:false}); 
//************************************COMPLEX PRICE***************************************************
    jQuery("#price_complex").jqGrid(
      { url:      'get_complex_content', 
        datatype: "json", 
        height: 550, 
        width: 700,
        colNames:['id','nomenclature', 'manufacturer', 'price','prices','qnt','series', 'qnt_order'], 
        colModel:[ 
          {name:'id',           index:'id',            width:5, hidden:true}, //, hidden:true
          {name:'nomenclature', index:'nomenclature',  searchoptions:{sopt:['eq','bw','bn','cn','nc','ew','en']}} ,
           {name:'manufacturer', index:'manufacturer'},
           {name:'price',        index:'price', align:"right", width:40}, 
           {name:'prices_id',       index:'prices_id', align:"right", hidden:true}, //, hidden:true
           {name:'qnt',             index:'qnt', align:"right", width:40}, 
           {name:'series_id',       index:'series_id'},
           {name:'qnt_order',       index:'qnt_order', width:40, editable:true}
          ], 
          rowNum:10, 
          rowList:[10,20,30], 
          pager: '#pager_to_complex', 
          sortname: 'id', 
          onSelectRow: function(id)
            { 
              if(id && id!==lastsel)
              { 
                jQuery('#price_complex').jqGrid('restoreRow',lastsel); 
                jQuery('#price_complex').jqGrid('editRow',id,true); 
                lastsel=id; 
              } 
            }, 
          viewrecords: true, 
          sortorder: "desc", 
          jsonReader: {
              repeatitems: false, 
              root : "rows",
              page : "page",
              total: "total",
              records : "records",
              id    : "0"
            },
          editurl: "complex_price/save_changes", 
          caption:"Price data" }); 
    jQuery("#price_complex").jqGrid('navGrid','#pager_to_complex',{edit:true,add:false,del:false}); 
    jQuery("#price_complex").jqGrid('filterToolbar',{searchOperators : true});
 //});
};

(function($) {
  $(document).ready(function() {
    if(!$("form").is("#new_price")) {
      get_data (window.location);
    }
  }   
)})(jQuery);


// (function($) {
//   // 1. вешаем обработчик
//   $(document).ready(function() {
//     $('#ul_pagination').onclick = function(e) {
   
//     // 2. получаем event.target
//     var event = e || window.event;
//     var target = event.target || event.srcElement;
   
//     // 3. проверим, интересует ли нас этот клик?
//     // если клик был не на ссылке, то нет
//     if (target.tagName != 'A') return;
     
//     // обработать клик по ссылке
//     var id = target.getAttribute('id');
//     alert(id); // в данном примере просто выводим
//     return false;
//   };
// });
//  })(jQuery);

function blocking_user(domElement)
{
  // alert( domElement.text() );
  // alert( $(this).text() );
  $.ajax(
    {
      type: "GET",
      url: "/users/" + domElement.attr("user_id") + "/blocking",
      datatype: "text",
      error: function(XMLHttpRequest, textStatus, errorThrow)
      {
        alert("No answer from server!");
      },
      success: function(result)
      {
        domElement.text(result);
      }
    });
}

(function($) {
  $(document).ready(function() {
    notif("Success reset!");
    console.log( "Insert handler" );
    $("a.user_block").each(
      function(index) {
        //alert( index + ": " + $(this).text() );
        $(this).click(function(){
         //alert("hello :" + $(this).attr("id"));
         blocking_user($(this));
         return false;
        }
      );
    });
})}) (jQuery);


function notif(msg)
{
  $.noty.defaults = {
    layout: 'top',
    theme: 'defaultTheme',
    type: 'alert',
    text: msg,
    dismissQueue: true, // If you want to use queue feature set this true
    template: '<div class="noty_message"><span class="noty_text"></span><div class="noty_close"></div></div>',
    animation: {
        open: {height: 'toggle'},
        close: {height: 'toggle'},
        easing: 'swing',
        speed: 500 // opening & closing animation speed
    },
    timeout: 1000, // delay for closing event. Set false for sticky notifications
    force: false, // adds notification to the beginning of queue when set to true
    modal: false,
    maxVisible: 5, // you can set max visible notification for dismissQueue true option
    closeWith: ['click'], // ['click', 'button', 'hover']
    callback: {
        onShow: function() {},
        afterShow: function() {},
        onClose: function() {},
        afterClose: function() {}
    },
    buttons: false // an array of buttons
};
}


function reset_order()
{
  $.ajax(
  {
    type: "GET",
    url: "/complex_price/reset_order",
    datatype: "text",
    error: function(XMLHttpRequest, textStatus, errorThrow)
    {
      //$(this).text("Error!!!");
       $('#noty_div').noty({text: 'Ошибка: ответ от сервера не получен!'}); //TODO:wrire universal function!
    },
    success: function(result)
    {
      //$(this).text("Ok");
      //notif("Success reset!")
      $('#noty_div').noty({text: 'Заказ отчищен!'});
      $('#price_complex').trigger( 'reloadGrid' );;
    }
  });
}
// var timeoutHnd;
// var flAuto = true;


// function gridReload()
// {
//   var nm_mask = jQuery("#item").val();
//   var cd_mask = jQuery("#search_cd").val();
//   jQuery("#list4").jqGrid('setGridParam', {url:"get_data?nm_mask="+nm_mask+"&cd_mask="+cd_mask,page:1}).trigger("reloadGrid");
// }

// function enableAutosubmit(state)
// {
//   flAuto = true;
//   jQuery("#submitButton").attr("disabled", flAuto);
// }

// function doSearch(ev)
// {
//   if(!flAuto)
//     return;
//   if(timeoutHnd)
//     clearTimeout(timeoutHnd);
//   timeoutHnd = setTimeout(gridReload, 500);
// }