class SessionsController < ApplicationController
  layout 'app'
  skip_before_filter :require_login, :only => [:new, :create]
  
  def new
  end

  def create
    user = login(params[:email], params[:password], params[:remember_me])
    if user
      redirect_back_or_to dashboard_path, :notice => t(:login_in)
    else
      flash.now.alert = t(:login_error)
      render :new
    end
  end

  def destroy
    logout
    redirect_to root_url
  end
end
