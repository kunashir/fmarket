class PagesController < ApplicationController
  skip_before_filter :require_login, :only => [:home, :about, :help]
  def home
  end

  def about
  end

  def help
  end
end
