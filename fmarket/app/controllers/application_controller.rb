class ApplicationController < ActionController::Base
  before_filter :require_login
  protect_from_forgery
  layout "app"
  include ApplicationHelper


  def require_login
    unless logged_in?
      flash[:error] = "You must be logged in to access this section"
      redirect_to new_session_path # прерывает цикл запроса
    end
  end
end
