class CompaniesController < ApplicationController
  skip_before_filter :require_login, :only => [:new, :create]
  # GET /companies
  # GET /companies.json
  def subdivisions
    parent = params[:parent] #TODO: add check to correct data
    @companies = Company.find(parent).children
    respond_to do |format|
      format.html { render :index} # show.html.erb
      format.json { render json: @company }
    end
  end

  def index
    @companies = Company.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @companies }
    end
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
    @company = Company.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/new
  # GET /companies/new.json
  def new
    parent = (Company.find(params[:parent]) if params[:parent]) || nil
    @company = Company.new
    if parent #init subdiv
      @company.parent = parent
      @company.inn = parent.inn
      @company.buyer = parent.buyer
      @company.supplier = parent.supplier
    else
      flash[:info] = t(:step1_new_company)
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @company }
    end
  end

  # GET /companies/1/edit
  def edit
    @company = Company.find(params[:id])
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(params[:company])

    respond_to do |format|
      if @company.valid?#save
        # if !current_user.company
        #   current_user.company = @company
        #   current_user.save
        # end
        #@company.save
        format.html { redirect_to new_user_path(@company)}
        session[:new_company] = @company
        format.json { render json: @company, status: :created, location: @company }
      else
        format.html { render action: "new" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /companies/1
  # PUT /companies/1.json
  def update
    @company = Company.find(params[:id])

    respond_to do |format|
      if @company.update_attributes(params[:company])
        format.html { redirect_to @company}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company = Company.find(params[:id])
    @company.destroy

    respond_to do |format|
      format.html { redirect_to companies_url }
      format.json { head :no_content }
    end
  end
end





