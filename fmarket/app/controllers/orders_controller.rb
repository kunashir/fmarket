class OrdersController < ApplicationController
	def orders_for_contractors
		@orders = Order.for_contractors(current_user.company)
		respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @orders }
    end
	end

	def orders_from_customers
		@orders = Order.from_customers(current_user.company)
		respond_to do |format|
    	format.html # index.html.erb
    	format.json { render json: @orders }
    end
	end

	def create_orders # call from complex price
		storage = session[:order] || {}
		if storage.size == 0
			flash[:error] = t(:no_data_to_order)
			redirect_to orders_for_contractors_path
			return
		end
		Order.create_orders(current_user.company, storage)
		flash[:success] = t(:order_created)
		redirect_to orders_for_contractors_path
	end

	def set_status
	end 

   #GET get_content
  def get_content
    rows = params[:rows].to_i
    page = params[:page].to_i
    start_index = rows*(page - 1)
    end_index   = page*rows
    @order = Order.find(params[:id])
    if @order
      arr_contetn = @order.get_content
      respond_to do |format|
        output = arr_contetn[start_index, rows]
        total_page = arr_contetn.size/rows
        output_hash = { :page => page, :total => total_page, :records => arr_contetn.size, :rows => output}
        format.json { render json: output_hash }
      end
    end
  end

  def show
    @order = Order.find(params[:id])
  end

  def get_xml
    @order = Order.find(params[:id])
    output_file = @order.create_xml
    headers['Content-Type'] = "application/xml"
    headers['Content-Disposition'] = "attachment; filename=#{File.basename(output_file).inspect}"
    render file: File.absolute_path(output_file), layout: false
  end
end
