# coding: utf-8
class PricesController < ApplicationController
  before_filter :require_login
  # GET /prices
  # GET /prices.json
  def index
    @prices = Price.own_prices(current_user.company)

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @prices }
    end
  end

  # GET /prices/1
  # GET /prices/1.json
  def show
    puts "=================PRICE SHOW ================="
    @price = Price.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @price }
    end
  end

  # GET /prices/new
  # GET /prices/new.json
  def new
    @price = Price.new
    puts "IT NEW ACTIONS"
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @price }
    end
  end

  # GET /prices/1/edit
  def edit
    @price = Price.find(params[:id])
  end

  # POST /prices
  # POST /prices.json
  def create
    original_file_name = save_file(params[:file])
     
    @price = Price.new(params[:price])
    @price.company = current_user.company
    respond_to do |format|
      if @price.save
        puts "PRICE SAVE======================="
        js_code = "get_data('#{price_path(@price)}');"
        puts js_code + "============================JS"
        flash.now.alert = t(:price_save_success)
        @price.load_from_file(original_file_name)
        
        format.html do
         
          #redirect_to @price, notice: 'Price was successfully created.' 
          redirect_to prices_url
        end
        format.json { render json: {path: @price, success: true }}
        format.js {render js: js_code}
      else
        puts "PRICE SAVE ERROR======================="
        flash.now.alert = t(:error_save_price)
        format.html do
          flash.now.alert = t(:error_save_price)
          render action: "new", notice: t(:error_save_price) 
        end
        format.json { render json: {path: "", success: false }}
      end
    end
  end

  # PUT /prices/1
  # PUT /prices/1.json
  def update
    @price = Price.find(params[:id])
    original_file_name = save_file(params[:file]) if params[:file]
    @price.company = current_user.company
    respond_to do |format|
      puts "Format is #{format}"
      if @price.update_attributes!(params[:price])
        @price.load_from_file(original_file_name) if original_file_name 
        format.html { redirect_to prices_url, notice: 'Price was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @price.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /prices/1
  # DELETE /prices/1.json
  def destroy
    @price = Price.find(params[:id])
    @price.destroy

    respond_to do |format|
      format.html { redirect_to prices_url }
      format.json { head :no_content }
    end
  end

  #GET get_content
  def get_content
    puts "PRICE CONTENT"
    rows  = params[:rows].to_i
    page = params[:page].to_i
    start_index = rows*(page - 1)
    end_index   = page*rows
    @price = Price.find(params[:id])
    if @price
      arr_contetn = @price.get_content
      respond_to do |format|
        output = arr_contetn[start_index, rows]
        total_page = arr_contetn.size/rows
        output_hash = { :page => page, :total => total_page, :records => arr_contetn.size, :rows => output}
        format.json { render json: output_hash }
      end
    end
  end

  def get_complex_content
    #Get all valid prices for company
    #and render Grid with able to ordering 
    arr_valid_prices = Price.get_prices(current_user.company)
    rows  = params[:rows].to_i
    page = params[:page].to_i
    search = params[:_search]
    momenclature_mask = params[:nomenclature] || "%"
    manufacturer_mask = params[:manufacturer] || "%"
    qnt_mask          = params[:qnt_order].to_i || 0

    start_index = rows*(page - 1)
    end_index   = page*rows

    storage = session[:order] || {} #maybe were reboot => restore order
    puts storage
    if arr_valid_prices
      output = []
      arr_valid_prices.each  do |price|
        cur_content = price.get_content(:nomenclature => momenclature_mask)
        cur_content.each do |item| 
          item[:qnt_order] = storage[item["id"]] if storage.has_key?(item["id"])
          if (qnt_mask != 0) and (!storage.has_key?(item["id"]))
            next
          end

          output<<item
        end
      end 
      total_page = output.size/rows
      output_hash = { :page => page, :total => total_page, :records => output.size, :rows => output[start_index, end_index]}
      puts output_hash
      respond_to do |format|
        format.json { render json: output_hash }
      end
    end 
  end

  def reset_order
    session[:order] = {}
    #get_complex_content
    render json: {:success => "Ok"}
  end

  def save_changes 
    #save new count of order to storage
    #order - it is hash, when key is id of content of price!
    storage = session[:order] || {}
    price_content_id  = params[:id].to_i
    qnt_order         = params[:qnt_order].to_i
    storage[price_content_id] = qnt_order
    session[:order] = storage
    puts storage
    respond_to do |format|
      format.json { render json: {:success => "Ok"}}
    end

  end

private

 

end
