class UsersController < ApplicationController
  #before_filter :requre_login
  skip_before_filter :require_login, :only => [:new, :create]
  
  def blocking
    if !current_user.admin?
      flash[:error] = t(:dont_may_blocking)
      redirect_to users_path
      return
    end
    @user = User.find(params[:id])
    if @user.toggle! :block
      #flash[:success] = t(:blocking_success)
    else
      flash[:error] = t(:blocking_error)
    end
    #redirect_to users_path
    render :text => @user.string_status
  end

  def new
    #redirect_to current_users if current_users 
    if session[:new_company]
      flash[:info] = t(:step2_new_user)
    end
    @user = User.new
  end

  def show
    if !current_user.admin?
      @user = current_user
    else
      @user = User.find(params[:id])
    end
  end

  def index
    page = params[:page] || 1
    if !current_user.admin?
      unless current_user.company.is_head? 
        redirect_to current_user
        return 
      end
      
      @users = User.users_company(current_user.company)
    else
      @users = User.page(1) #.order(:email).page page
    end
     
  end

  def create
    
    @user = User.new(params[:user])
    @user.company = session[:new_company] || nil
    if @user.save
      @user.company.save!
      auto_login(@user)
      flash[:success] = t(:sing_up_success)
      redirect_to @user
    else
      #if user's creating has failed - del company too
      #new_comp = session[:new_company]
      #new_comp.delete
      flash[:error] = t(:sign_up_error)
      render :new
    end 
  end

  def dashboard
    @user = current_user
  end

  def destroy
    if !current_user.admin?
      redirect_to users_path
    end
    User.find(params[:id]).destroy
    flash[:success] = t(:delete_success)
    redirect_to users_path
  end
end
