class CreateManufacturers < ActiveRecord::Migration
  def change
    create_table :manufacturers do |t|
      t.string :name
      t.integer :worldclassifier_id
      t.integer :code

      t.timestamps
    end
  end
end
