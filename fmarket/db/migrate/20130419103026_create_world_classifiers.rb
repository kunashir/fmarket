class CreateWorldClassifiers < ActiveRecord::Migration
  def change
    create_table :world_classifiers do |t|
      t.string :name
      t.string :full_name
      t.string :alpha2

      t.timestamps
    end
    add_index :world_classifiers, :name
  end
end
