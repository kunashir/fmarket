class CreateExtraInfos < ActiveRecord::Migration
  def change
    create_table :extra_infos do |t|
      t.string :name, :nil => false
      t.integer :nomenclature_id, :nil => false

      t.timestamps
    end
  end
end
