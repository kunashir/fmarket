class AddBlockToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :block, :boolean, :default => true
  end
end
