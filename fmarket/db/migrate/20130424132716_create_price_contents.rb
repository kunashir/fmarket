class CreatePriceContents < ActiveRecord::Migration
  def change
    create_table :price_contents do |t|
      t.integer :prices_id,       :nil => false
      t.integer :nomenclature_id, :nil => false
      t.integer :series_id,       :nil => false
      t.decimal :price,            :nil => false
      t.decimal :qnt
      t.integer :manufacture_id

      t.timestamps
    end
  end
end
