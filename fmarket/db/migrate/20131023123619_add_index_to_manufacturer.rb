class AddIndexToManufacturer < ActiveRecord::Migration
  def change
    add_index :manufacturers, :name
    add_index :manufacturers, :worldclassifier_id
  end
end
