class CreateSeries < ActiveRecord::Migration
  def change
    create_table :series do |t|
      t.integer :nomenclature_id, :nil => false
      t.string :serial_code, :nil => false
      t.date :expiration_date, :nil => false

      t.timestamps
    end
  end
end
