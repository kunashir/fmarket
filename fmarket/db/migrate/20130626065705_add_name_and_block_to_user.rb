class AddNameAndBlockToUser < ActiveRecord::Migration
  def change
    add_column :users, :name, :string
    add_column :users, :block, :boolean, :default => true
  end
end
