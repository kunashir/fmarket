class AddIndexToNomenclature < ActiveRecord::Migration
  def change
    add_index :nomenclatures, :name
    add_index :nomenclatures, :company_id
  end
end
