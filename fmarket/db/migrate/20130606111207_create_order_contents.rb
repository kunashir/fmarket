class CreateOrderContents < ActiveRecord::Migration
  def change
    create_table :order_contents do |t|
      t.integer :order_id
      t.integer :price_content_id
      t.decimal :qnt

      t.timestamps
    end
  end
end
