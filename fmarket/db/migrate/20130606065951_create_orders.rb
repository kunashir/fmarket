class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :owner_id
      t.integer :contractor_id
      t.datetime :date
      t.integer :status

      t.timestamps
    end
  end
end
