class AddSubCodeToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :subcode, :string, :default => '000'
  end
end
