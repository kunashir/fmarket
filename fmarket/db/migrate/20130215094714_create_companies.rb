class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :inn
      t.string :name
      t.boolean :supplier
      t.boolean :buyer
      t.integer :parent_id
      t.integer :lft
      t.integer :rgt
      t.integer :depth

      t.timestamps
    end
	add_index :companies, :name, :unique => true
	add_index :companies, :inn, :unique => true
  end
  
  def self.down
    drop_table :companies
  end
end
