# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131023123619) do

  create_table "companies", :force => true do |t|
    t.string   "inn"
    t.string   "name"
    t.boolean  "supplier"
    t.boolean  "buyer"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.integer  "depth"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.boolean  "block",      :default => true
    t.string   "subcode",    :default => "000"
  end

  create_table "extra_infos", :force => true do |t|
    t.string   "name"
    t.integer  "nomenclature_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "manufacturers", :force => true do |t|
    t.string   "name"
    t.integer  "worldclassifier_id"
    t.integer  "code"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "manufacturers", ["name"], :name => "index_manufacturers_on_name"
  add_index "manufacturers", ["worldclassifier_id"], :name => "index_manufacturers_on_worldclassifier_id"

  create_table "nomenclatures", :force => true do |t|
    t.string   "name",       :null => false
    t.integer  "company_id", :null => false
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "nomenclatures", ["company_id"], :name => "index_nomenclatures_on_company_id"
  add_index "nomenclatures", ["name"], :name => "index_nomenclatures_on_name"

  create_table "order_contents", :force => true do |t|
    t.integer  "order_id"
    t.integer  "price_content_id"
    t.decimal  "qnt"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "orders", :force => true do |t|
    t.integer  "owner_id"
    t.integer  "contractor_id"
    t.datetime "date"
    t.integer  "status"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "price_contents", :force => true do |t|
    t.integer  "prices_id"
    t.integer  "nomenclature_id"
    t.integer  "series_id"
    t.decimal  "price"
    t.decimal  "qnt"
    t.integer  "manufacture_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "prices", :force => true do |t|
    t.date     "start_date"
    t.date     "end_date"
    t.integer  "company_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "reciver_id"
  end

  create_table "series", :force => true do |t|
    t.integer  "nomenclature_id"
    t.string   "serial_code"
    t.date     "expiration_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                        :default => "f"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "company_id",                   :default => 0,    :null => false
    t.boolean  "admin"
    t.string   "remember_me_token"
    t.time     "remember_me_token_expires_at"
    t.string   "name"
    t.boolean  "block",                        :default => true
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

  create_table "world_classifiers", :force => true do |t|
    t.string   "name"
    t.string   "full_name"
    t.string   "alpha2"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "world_classifiers", ["name"], :name => "index_world_classifiers_on_name"

end
