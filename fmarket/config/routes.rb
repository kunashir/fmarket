Fmarket::Application.routes.draw do
  get "pages/home" => 'pages#home',   :as => :home_path

  get "pages/about" => 'pages#about',  :as => :about_path

  get "pages/help" => 'pages#help',    :as => :help_path

  match "/signin",                      :to => "sessions#new",      :as => :signin
  match "/signup",                      :to => "companies#new",         :as => :signup #"users#new" 
  match "/logout",                      :to => "sessions#destroy",  :as => :logout
  match "/dashboard",                   :to => "users#dashboard",   :as => :dashboard
  match "/subdivisions",                :to => "companies#subdivisions", :as => :subdivisions
  match "/complex_price",               :to => "prices#complex_price", :as => :complex_price
  match "/get_complex_content",         :to => "prices#get_complex_content",  :as => :complec_content
  match "/complex_price/save_changes",  :to => "prices#save_changes",  :as => :save_changes
  match "/complex_price/reset_order",   :to => "prices#reset_order",  :as => :reset_order
  match "/orders_for_contractors",      :to => "orders#orders_for_contractors", :as => :orders_for_contractors 
  match "/create_orders",               :to => "orders#create_orders", :as => :create_orders
  match "/orders_from_customers",      :to => "orders#orders_from_customers", :as => :orders_from_customers 
  resources :prices do
    member do
      get 'get_content'
      #get 'complex_price'
    end
  end

  resources :companies
  
  resources :orders do
    member do
      get 'get_content'
      get 'get_xml'
    end
  end

  resources :sessions, :only => [:new, :create, :destroy]
  
  resources :users do 
    member do 
      get 'blocking'
    end
  end

  root :to => 'pages#home'

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
